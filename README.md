# Web Server

## Requirements

npm install

## How to run

nodemon src/app.js -e js,hbs

## Access through port 3000

http://localhost:3000/

## Routes

- http://localhost:3000/ — where you can enter and search for a location to get the geocode and forecast
- http://localhost:3000/about - includes a photo of who created the app
- http://localhost:3000/help - shows an empty help page
