console.log('Client side JavaScript file is loaded...')

const weatherForm = document.querySelector('form')
const searchElement = document.querySelector('input')

const messageOne = document.querySelector('#message-1')
const messageTwo = document.querySelector('#message-2')

weatherForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const location = searchElement.value
    messageOne.textContent = 'Loading Location...'
    messageTwo.textContent = 'Loading Forecast...'

    fetch('http://localhost:3000/weather?address=' + location).then((response) => {
        response.json().then((data) => {
            if (data.error) {
                messageOne.textContent = data.error
                messageTwo.textContent = ""
            } else {
                messageOne.textContent = "Location: " + data.location
                console.log(data.location)
                messageTwo.textContent = "Forecast: " + data.forecast
                console.log(data.forecast)
            }
        })
    })
})